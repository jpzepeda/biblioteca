package com.example.biblioteca;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.biblioteca.data.models.Person;

public class UserViewModel extends ViewModel {

    private MutableLiveData<Person> mPerson;

    public UserViewModel() {
        mPerson = new MutableLiveData<>();
    }
}
