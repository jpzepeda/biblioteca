package com.example.biblioteca.ui.copies;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biblioteca.R;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Book;
import com.example.biblioteca.data.models.Copy;
import com.example.biblioteca.ui.adapters.ListAdapterBooks;
import com.example.biblioteca.ui.adapters.ListAdapterCopies;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class CopiesFragment extends Fragment {

    private CopiesViewModel copiesViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //Return root
        return inflater.inflate(R.layout.fragment_copies, container, false);
    }

    private void init(@NotNull View view) {
        final DataManager dataManager = new DataManager(requireActivity());
        ArrayList<Copy> copies = dataManager.getAllCopies();
        ListAdapterCopies listAdapterCopies = new ListAdapterCopies(copies,getActivity());
        RecyclerView recyclerView = view.findViewById(R.id.copiesRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(listAdapterCopies);


        FloatingActionButton fab = view.findViewById(R.id.copiesFab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //int id = 0;
                Snackbar.make(view, R.string.add_new_copy, Snackbar.LENGTH_LONG)
                        .setAction(R.string.action, null).show();
                //Bundle bundle = new Bundle(1);
                //bundle.putFloat("id",id);
                Navigation.findNavController(view).navigate(R.id.editCopiesFragment);
                //Navigation.findNavController(view).navigate(R.id.personsEdit,bundle);
            }
        });
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);
    }
}