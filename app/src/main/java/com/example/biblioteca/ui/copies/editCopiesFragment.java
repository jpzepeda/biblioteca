package com.example.biblioteca.ui.copies;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.example.biblioteca.R;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Person;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class editCopiesFragment extends Fragment {

    private EditCopiesViewModel mViewModel;

    public static editCopiesFragment newInstance() {
        return new editCopiesFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_copies_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View view) {
        //Getting data from bundle
        int id = 0;

        if (getArguments() != null) {
            Log.i("argument", String.valueOf(requireArguments().getFloat("id")));
        }

        if (getArguments() != null) {
            id = (int) getArguments().getFloat("id");
        }

        final DataManager dataManager = new DataManager(requireActivity());
        AutoCompleteTextView autoCompleteTextView = view.findViewById(R.id.autoCompleteState);
        //The Calendar
        final Date[] date = new Date[1];

        //Array adapter
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(requireContext(), R.array.estates, android.R.layout.select_dialog_singlechoice);
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setAdapter(arrayAdapter);

        //Cancel and save buttons
        Button cancelButtonPerson = requireActivity().findViewById(R.id.cancel_button_person);
        Button saveButtonPerson = requireActivity().findViewById(R.id.accept_button_person);
        //Debug text field
        TextView debug = requireActivity().findViewById(R.id.textView2);
        Person person = null;
        //Replaced with method to get only one
        if(id > 0)
            person = dataManager.getPerson(id);
        //Text Fields
        TextInputLayout textFieldName = requireActivity().findViewById(R.id.textFieldName);
        TextInputLayout textFieldLastName1 = requireActivity().findViewById(R.id.textFieldLastName1);
        TextInputLayout textFieldLastName2 = requireActivity().findViewById(R.id.textFieldLastName2);
        TextInputLayout textFieldPhone = requireActivity().findViewById(R.id.textFieldPhone);
        TextInputLayout textFieldEmail = requireActivity().findViewById(R.id.textFieldEmail);
        TextInputLayout textFieldID = requireActivity().findViewById(R.id.textFieldID);
        TextInputLayout textFieldDate = requireActivity().findViewById(R.id.textFieldDate);
        //Prefiil Fields with object
        if (id > 0 && person != null) {

            debug.setText(String.valueOf(id));
            Objects.requireNonNull(textFieldName.getEditText()).setText(person.getName());
            Objects.requireNonNull(textFieldLastName1.getEditText()).setText(person.getLastName1());
            Objects.requireNonNull(textFieldLastName2.getEditText()).setText(person.getLastName2());
            Objects.requireNonNull(textFieldPhone.getEditText()).setText(person.getPhone());
            Objects.requireNonNull(textFieldEmail.getEditText()).setText(person.getEmail());
            Objects.requireNonNull(textFieldID.getEditText()).setText(person.getClaveElector());
            date[0] = person.getBirthDate();
            Locale spanish = new Locale("es", "ES");
            SimpleDateFormat simpleFormat = new SimpleDateFormat("MM/dd/yyyy", spanish);
            String dateToString = simpleFormat.format(date[0]);
            Objects.requireNonNull(textFieldDate.getEditText()).setText(dateToString);
            autoCompleteTextView.setText(dataManager.getPersonState(person.getIdState()));
        }
        //textFieldDate.getEditText().setFocusable(false);
        Button datePickButton = requireActivity().findViewById(R.id.pick_date_button);
        //Objects.requireNonNull(textFieldName.getEditText()).setText(R.string.name_fillr);
        MaterialDatePicker.Builder materialDateBuilder = MaterialDatePicker.Builder.datePicker();
        materialDateBuilder.setTitleText(R.string.select_date_picker);
        final MaterialDatePicker materialDatePicker = materialDateBuilder.build();
        //Date Picker button
        datePickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                materialDatePicker.show(requireActivity().getSupportFragmentManager(), "MATERIAL_DATE_PICKER");
            }
        });
        textFieldDate.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                materialDatePicker.show(requireActivity().getSupportFragmentManager(), "MATERIAL_DATE_PICKER");
            }
        });
        materialDatePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Object selection) {
                Locale spanish = new Locale("es", "ES");
                // Get the offset from our timezone and UTC.
                TimeZone timeZoneUTC = TimeZone.getDefault();
                // It will be negative, so that's the -1
                int offsetFromUTC = timeZoneUTC.getOffset(new Date().getTime()) * -1;
                date[0] = new Date(((Long) Objects.requireNonNull(materialDatePicker.getSelection())) + offsetFromUTC);
                SimpleDateFormat simpleFormat = new SimpleDateFormat("MM/dd/yyyy", spanish);
                String dateToString = simpleFormat.format(date[0]);
                Objects.requireNonNull(textFieldDate.getEditText()).setText(dateToString);
            }
        });
        //Verification
        cancelButtonPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.nav_persons);
            }
        });

        int finalId = id;
        saveButtonPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Objects.requireNonNull(textFieldEmail.getEditText()).setError(null);
                Objects.requireNonNull(textFieldPhone.getEditText()).setError(null);
                autoCompleteTextView.setError(null);
                if (textFieldName.getEditText().getText().toString().equals("") || textFieldLastName1.getEditText().getText().toString().equals("") || textFieldLastName2.getEditText().getText().toString().equals("") || textFieldPhone.getEditText().getText().toString().equals("") || textFieldEmail.getEditText().getText().toString().equals("") || textFieldDate.getEditText().getText().toString().equals("") || textFieldID.getEditText().getText().toString().equals("") || autoCompleteTextView.getText().toString().equals("")) {
                    Snackbar.make(view, R.string.must_fill_all_fields, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Verify email
                Pattern pattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
                Matcher mat = pattern.matcher(Objects.requireNonNull(textFieldEmail.getEditText()).getText());
                if (!mat.matches()) {
                    textFieldEmail.getEditText().setError(getString(R.string.email_example));
                    Snackbar.make(view, R.string.not_valid_email, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Verify phone
                pattern = Pattern.compile("[0-9]{10}");
                mat = pattern.matcher(Objects.requireNonNull(textFieldPhone.getEditText()).getText());
                if (!mat.matches()) {
                    textFieldPhone.getEditText().setError(getString(R.string.not_valid_phone));
                    Snackbar.make(view, R.string.not_valid_phone, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Verify state
                int cState = 0;
                for (int i = 0; i < arrayAdapter.getCount(); i++) {
                    if (autoCompleteTextView.getText().toString().compareTo(arrayAdapter.getItem(i).toString()) == 0)
                        cState++;
                }
                if (cState != 1) {
                    autoCompleteTextView.setError(getString(R.string.not_valid_state));
                    Snackbar.make(view, R.string.not_valid_state, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //State by number
                Log.i(String.valueOf(R.string.action), autoCompleteTextView.getText().toString());
                String[] states = getResources().getStringArray(R.array.estates);
                final int state = Arrays.asList(states).indexOf(autoCompleteTextView.getText().toString()) + 1;
                //Verify ID
                pattern = Pattern.compile("[A-Z]{6}+[0-9]{08}+[HM]+[0-9]{3}");
                mat = pattern.matcher(Objects.requireNonNull(textFieldID.getEditText()).getText());
                if (!mat.matches()) {
                    textFieldID.getEditText().setError(getString(R.string.incorrect_format));
                    Snackbar.make(view, R.string.incorrect_format, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Here we add the new person
                Person person = new Person(finalId,
                        textFieldName.getEditText().getText().toString(),
                        textFieldLastName1.getEditText().getText().toString(),
                        textFieldLastName2.getEditText().getText().toString(),
                        textFieldID.getEditText().getText().toString(),
                        state,
                        date[0],
                        date[0],
                        textFieldPhone.getEditText().getText().toString(),
                        textFieldEmail.getEditText().getText().toString(),
                        "pollito");
                Log.i(String.valueOf(R.string.action), "Aqui");
                Log.i(String.valueOf(R.string.action), person.getName());
                Log.i(String.valueOf(R.string.action), person.getBirthDate().toString());
                if (finalId > 0)
                    dataManager.editPerson(person);
                else
                    dataManager.addPerson(person);
                Navigation.findNavController(view).navigate(R.id.nav_persons);
            }
        });

    }

}