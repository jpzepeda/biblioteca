package com.example.biblioteca.ui.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biblioteca.R;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Log;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ListAdapterLogging extends RecyclerView.Adapter<ListAdapterLogging.ViewHolder> {
    private final String[] codes;
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Log> mdata;
    //Recycler constructor
    public ListAdapterLogging(List<Log> itemList, Context context){
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.mdata = itemList;
        codes = new String[]{"Create","Read","Update","Update"};
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    @Override
    public ListAdapterLogging.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = layoutInflater.inflate(R.layout.list_element_log,null);
        return new ListAdapterLogging.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final ListAdapterLogging.ViewHolder holder, final int position) {
        holder.bindData(mdata.get(position));
    }

    public void setItems(List<Log> items) {
        mdata = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        //Declaring elements
        TextView primaryText; //Author

        ViewHolder(View itemView) {
            super(itemView);
            primaryText = itemView.findViewById(R.id.cardMainText);
        }

        //Get The Data
        public void bindData(@NotNull final Log log) {
            //Getting data manager
            DataManager dataManager = new DataManager(context);

            //Printing data
            primaryText.setText(String.format("Tabla %s operacion %s, Id:%s", log.getTableName(),codes[log.getType()],log.getPk()));

        }
    }
}
