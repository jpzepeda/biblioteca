package com.example.biblioteca.ui.authors;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biblioteca.R;
import com.example.biblioteca.UserViewModel;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Author;
import com.example.biblioteca.data.models.Copy;
import com.example.biblioteca.ui.adapters.ListAdapterAuthors;
import com.example.biblioteca.ui.adapters.ListAdapterCopies;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

public class AuthorsFragment extends Fragment {

    private AuthorsViewModel authorsViewModel;

    private UserViewModel userViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //Return root
        View root = inflater.inflate(R.layout.fragment_authors, container, false);
        return root;
    }

    private void init(@NotNull View view) {
        final DataManager dataManager = new DataManager(requireActivity());
        ArrayList<Author> authors = dataManager.getAllAuthors();
        ListAdapterAuthors listAdapterAuthors = new ListAdapterAuthors(authors,getActivity());
        RecyclerView recyclerView = view.findViewById(R.id.authorsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(listAdapterAuthors);


        FloatingActionButton fab = view.findViewById(R.id.authorsFab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //int id = 0;
                Snackbar.make(view, R.string.add_new_author, Snackbar.LENGTH_LONG)
                        .setAction(R.string.action, null).show();
                //Bundle bundle = new Bundle(1);
                //bundle.putFloat("id",id);
                Navigation.findNavController(view).navigate(R.id.editAuthorsFragment);
                //Navigation.findNavController(view).navigate(R.id.personsEdit,bundle);
            }
        });
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i("Authors fragment","oh si");
        userViewModel = new ViewModelProvider(requireActivity()).get(UserViewModel.class);
        final NavController navController = Navigation.findNavController(view);
        SharedPreferences sharedPref = requireActivity().getPreferences(Context.MODE_PRIVATE);
        if(!sharedPref.contains(getString(R.string.user_id)))
            navController.navigate(R.id.loginFragment);
        init(view);
    }
}