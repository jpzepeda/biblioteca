package com.example.biblioteca.ui.books;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.example.biblioteca.R;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Book;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditBooksFragment extends Fragment {

    private EditBooksViewModel mViewModel;

    public static EditBooksFragment newInstance() {
        return new EditBooksFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_books_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(EditBooksViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View view) {
        //Getting data from bundle
        int id = 0;
        if(requireArguments() != null)
        id = (int) requireArguments().getFloat("id");
        Log.i("argument", String.valueOf(id));

        //Getting the data manager and the list of books
        final DataManager dataManager = new DataManager(requireActivity());

        //Auto complete text views (author, publisher and language adapters and auto complete text views
        AutoCompleteTextView autoCompleteTextViewAuthor = view.findViewById(R.id.autoCompleteAuthor);
        AutoCompleteTextView autoCompleteTextViewPublisher = view.findViewById(R.id.autoCompletePublisher);
        AutoCompleteTextView autoCompleteTextViewLanguage = view.findViewById(R.id.autoCompleteLanguage);

        //Cursor adapter setting ---------------------------
        ArrayAdapter<CharSequence> arrayAdapterAuthors = new ArrayAdapter(getContext(), android.R.layout.select_dialog_singlechoice,dataManager.getAllAuthorsAsString());
        ArrayAdapter<CharSequence> arrayAdapterPublishers = new ArrayAdapter(getContext(), android.R.layout.select_dialog_singlechoice,dataManager.getAllPublishersAsString());
        ArrayAdapter<CharSequence> arrayAdapterLanguages = new ArrayAdapter(getContext(), android.R.layout.select_dialog_singlechoice,dataManager.getAllLanguages());

        //Setting adapters to texts view's
        autoCompleteTextViewAuthor.setThreshold(1);
        autoCompleteTextViewAuthor.setAdapter(arrayAdapterAuthors);
        autoCompleteTextViewPublisher.setThreshold(1);
        autoCompleteTextViewPublisher.setAdapter(arrayAdapterPublishers);
        autoCompleteTextViewLanguage.setThreshold(1);
        autoCompleteTextViewLanguage.setAdapter(arrayAdapterLanguages);

        //Cancel and save buttons
        Button cancelButtonPerson = requireActivity().findViewById(R.id.cancel_button_book);
        Button saveButtonPerson = requireActivity().findViewById(R.id.accept_button_book);
        //Debug text field
        TextView debug = requireActivity().findViewById(R.id.textView2);

        //Declare Data ModelTo fill
        Book book = dataManager.getBook(id);

        //Text Fields
        TextInputLayout textFieldTitle = requireActivity().findViewById(R.id.textFieldTitle);
        TextInputLayout textFieldNumberPages = requireActivity().findViewById(R.id.textFieldNumberPages);
        TextInputLayout textFieldISBN = requireActivity().findViewById(R.id.textFieldISBN);
        TextInputLayout textFieldRareRate = requireActivity().findViewById(R.id.textFieldRareRate);

        //Prefix Fields with object
        if (id > 0 && book != null) {

            debug.setText(String.valueOf(id));
            //Text views
            Objects.requireNonNull(textFieldTitle.getEditText()).setText(book.getTitle());
            Objects.requireNonNull(textFieldNumberPages.getEditText()).setText(String.valueOf(book.getPages()));
            Objects.requireNonNull(textFieldISBN.getEditText()).setText(String.valueOf(book.getIsbn()));
            Objects.requireNonNull(textFieldRareRate.getEditText()).setText(String.valueOf(book.getRare()));
            //Auto complete text view
            autoCompleteTextViewAuthor.setText(dataManager.getAuthorName(book.getIdAuthor()));
            autoCompleteTextViewPublisher.setText(dataManager.getPublisherName(book.getIdPublisher()));
            autoCompleteTextViewLanguage.setText(dataManager.getLanguage(book.getIdLanguage()));
        }

        //Verification
        cancelButtonPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.nav_books);
            }
        });

        int finalId = id;

        saveButtonPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Disable errors
                Objects.requireNonNull(textFieldISBN.getEditText()).setError(null);
                Objects.requireNonNull(textFieldNumberPages.getEditText()).setError(null);
                Objects.requireNonNull(textFieldRareRate.getEditText()).setError(null);
                Objects.requireNonNull(autoCompleteTextViewAuthor).setError(null);
                Objects.requireNonNull(autoCompleteTextViewLanguage).setError(null);
                Objects.requireNonNull(autoCompleteTextViewPublisher).setError(null);
                //Verify all fields filled
                if (textFieldISBN.getEditText().getText().toString().equals("") || textFieldNumberPages.getEditText().getText().toString().equals("") || textFieldRareRate.getEditText().getText().toString().equals("") || autoCompleteTextViewAuthor.getText().toString().equals("") || autoCompleteTextViewLanguage.getText().toString().equals("") || autoCompleteTextViewPublisher.getText().toString().equals("") || textFieldTitle.getEditText().getText().toString().equals("")) {
                    Snackbar.make(view, R.string.must_fill_all_fields, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Verify number pages
                Pattern pattern = Pattern.compile("[0-9]*");
                Matcher mat = pattern.matcher(Objects.requireNonNull(textFieldNumberPages.getEditText()).getText());
                if (!mat.matches()) {
                    textFieldNumberPages.getEditText().setError(getString(R.string.pages_example));
                    Snackbar.make(view, R.string.pages_example, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Verify rare rate
                pattern = Pattern.compile("[0-9]{1}");
                mat = pattern.matcher(Objects.requireNonNull(textFieldRareRate.getEditText()).getText());
                if (!mat.matches()) {
                    textFieldRareRate.getEditText().setError(getString(R.string.rare_rate_example));
                    Snackbar.make(view, R.string.rare_rate_example, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Verify isbn
                pattern = Pattern.compile("[0-9]{10}");
                mat = pattern.matcher(Objects.requireNonNull(textFieldISBN.getEditText()).getText());
                if (!mat.matches()) {
                    textFieldISBN.getEditText().setError(getString(R.string.not_valid_isbn));
                    Snackbar.make(view, R.string.not_valid_isbn, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Verify publisher
                int cPublisher = 0;
                String[] publishersAsStrings = dataManager.getAllPublishersAsString();
                for (int i = 0; i < publishersAsStrings.length; i++) {
                    if (autoCompleteTextViewPublisher.getText().toString().compareTo(publishersAsStrings[i]) == 0)
                        cPublisher++;
                }
                if (cPublisher != 1) {
                    autoCompleteTextViewPublisher.setError(getString(R.string.not_valid_publisher));
                    Snackbar.make(view, R.string.not_valid_publisher, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Publisher by number
                final int publisherAsInt = Arrays.asList(publishersAsStrings).indexOf(autoCompleteTextViewPublisher.getText().toString()) + 1;
                Log.i("Publisher to enter",dataManager.getPublisherName(publisherAsInt));
                //Verify author
                int cAuthor = 0;
                String[] authorsAsStrings = dataManager.getAllAuthorsAsString();
                for (int i = 0; i < authorsAsStrings.length; i++) {
                    if (autoCompleteTextViewAuthor.getText().toString().compareTo(authorsAsStrings[i]) == 0)
                        cAuthor++;
                }
                if (cAuthor != 1) {
                    autoCompleteTextViewAuthor.setError(getString(R.string.not_valid_author));
                    Snackbar.make(view, R.string.not_valid_author, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Author by number
                final int authorAsInt = Arrays.asList(authorsAsStrings).indexOf(autoCompleteTextViewAuthor.getText().toString()) + 1;
                Log.i("Author to enter",dataManager.getAuthorName(publisherAsInt));
                //Verify Language
                int cLanguage = 0;
                String[] languagesAsStrings = dataManager.getAllLanguages();
                for (int i = 0; i < languagesAsStrings.length; i++) {
                    if (autoCompleteTextViewLanguage.getText().toString().compareTo(languagesAsStrings[i]) == 0)
                        cLanguage++;
                }
                if (cLanguage != 1) {
                    autoCompleteTextViewLanguage.setError(getString(R.string.not_valid_language));
                    Snackbar.make(view, R.string.not_valid_language, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                //Language by number
                final int languageAsInt = Arrays.asList(languagesAsStrings).indexOf(autoCompleteTextViewLanguage.getText().toString()) + 1;
                Log.i("Language to enter",dataManager.getLanguage(publisherAsInt));
                //Here we add the new person
                Book bookToEnter = new Book(finalId,
                        textFieldTitle.getEditText().getText().toString(),
                        publisherAsInt,
                        languageAsInt,
                        Integer.parseInt(textFieldNumberPages.getEditText().getText().toString()),
                        Integer.parseInt(textFieldISBN.getEditText().getText().toString()),
                        new Date(),
                        Integer.parseInt(textFieldRareRate.getEditText().getText().toString()),
                        authorAsInt
                );
                Log.i("Id para editar", String.valueOf(finalId));
                if (finalId > 0)
                    dataManager.editBook(bookToEnter);
                else
                    dataManager.addBook(bookToEnter);
                Navigation.findNavController(view).navigate(R.id.nav_books);
            }
        });

    }

}