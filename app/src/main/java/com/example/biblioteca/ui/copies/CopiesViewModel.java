package com.example.biblioteca.ui.copies;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CopiesViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CopiesViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is copies fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}