package com.example.biblioteca.ui.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biblioteca.R;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Book;
import com.example.biblioteca.data.models.Copy;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ListAdapterCopies extends RecyclerView.Adapter<ListAdapterCopies.ViewHolder> {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Copy> mdata;
    //Recycler constructor
    public ListAdapterCopies(List<Copy> itemList, Context context){
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.mdata = itemList;
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    @Override
    public ListAdapterCopies.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = layoutInflater.inflate(R.layout.list_element_copy,null);
        return new ListAdapterCopies.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final ListAdapterCopies.ViewHolder holder, final int position) {
        holder.bindData(mdata.get(position));
    }

    public void setItems(List<Copy> items) {
        mdata = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        //Declaring elements
        ImageView avatar;
        TextView primaryText; //Author
        TextView secondaryText; //Title
        TextView extraText; //Title
        Button edit;
        Button delete;

        ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.cardAvatar);
            primaryText = itemView.findViewById(R.id.cardMainText);
            secondaryText = itemView.findViewById(R.id.cardSecondaryText);
            edit = itemView.findViewById(R.id.linearLayoutCardViewButtonEdit);
            delete = itemView.findViewById(R.id.linearLayoutCardViewButtonDelete);
            extraText = itemView.findViewById(R.id.cardExtraText);
        }

        //Get The Data
        public void bindData(@NotNull final Copy copy) {
            //Getting data manager
            DataManager dataManager = new DataManager(context);

            //Printing data
            primaryText.setText(dataManager.getTitle(copy.getIdBook()));
            secondaryText.setText(dataManager.getCopiState(copy.getIdState()));
            extraText.setText(String.valueOf(copy.getEdicion()));

            //Buttons
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) copy.getId();
                    Snackbar.make(view, R.string.edit_copy, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    Bundle bundle = new Bundle(1);
                    bundle.putFloat("id",id);
                    Navigation.findNavController(view).navigate(R.id.editCopiesFragment,bundle);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) copy.getId();
                    Snackbar.make(view, R.string.delete_copy, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    dataManager.deleteCopy(copy);
                    mdata.remove(copy);
                    notifyDataSetChanged();
                }
            });
        }
    }
}
