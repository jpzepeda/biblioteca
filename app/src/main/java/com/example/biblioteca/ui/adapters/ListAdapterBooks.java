package com.example.biblioteca.ui.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biblioteca.R;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Book;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class ListAdapterBooks extends RecyclerView.Adapter<ListAdapterBooks.ViewHolder> {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Book> mdata;
    //Recycler constructor
    public ListAdapterBooks(List<Book> itemList, Context context){
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.mdata = itemList;
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    @Override
    public ListAdapterBooks.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = layoutInflater.inflate(R.layout.list_element_book,null);
        return new ListAdapterBooks.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final ListAdapterBooks.ViewHolder holder, final int position) {
        holder.bindData(mdata.get(position));
    }

    public void setItems(List<Book> items) {
        mdata = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        //Declaring elements
        ImageView avatar;
        TextView primaryText; //Author
        TextView secondaryText; //Title
        Button edit;
        Button delete;
        Button expand;
        TextView publisherText;
        TextView languageText;
        TextView numberPagesText;
        TextView isbnText;
        TextView rareRateText;
        LinearLayout expandableLayout;
        Boolean expandableVisivility = false;

        ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.cardAvatar);
            primaryText = itemView.findViewById(R.id.cardMainText);
            secondaryText = itemView.findViewById(R.id.cardSecondaryText);
            edit = itemView.findViewById(R.id.linearLayoutCardViewButtonEdit);
            delete = itemView.findViewById(R.id.linearLayoutCardViewButtonDelete);
            expand = itemView.findViewById(R.id.linearLayoutCardViewButtonExpand);
            //Expandable items
            publisherText = itemView.findViewById(R.id.cardEditorial);
            languageText = itemView.findViewById(R.id.cardLanguage);
            numberPagesText = itemView.findViewById(R.id.cardNumberPages);
            isbnText = itemView.findViewById(R.id.cardISBN);
            rareRateText = itemView.findViewById(R.id.cardRareRate);
            expandableLayout = itemView.findViewById(R.id.expandablePersonLayout);
        }

        //Get The Person
        public void bindData(@NotNull final Book book) {
            //Getting data manager
            DataManager dataManager = new DataManager(context);

            //Printing data
            primaryText.setText(book.getTitle());
            //Get author name
            secondaryText.setText(dataManager.getAuthorName(book.getIdAuthor()));
            //Get Publisher Name
            publisherText.setText(dataManager.getPublisherName(book.getIdPublisher()));
            //Get language name
            languageText.setText(dataManager.getLanguage(book.getIdLanguage()));
            numberPagesText.setText(String.valueOf(book.getPages()));
            isbnText.setText(String.valueOf(book.getIsbn()));
            rareRateText.setText(String.valueOf(book.getRare()));
            //Buttons
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) book.getId();
                    Snackbar.make(view, R.string.edit_book, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    Bundle bundle = new Bundle(1);
                    bundle.putFloat("id",id);
                    Log.i("mandado arg:", String.valueOf(id));
                    //Navigation.findNavController(view).navigate(R.id.personsEdit);
                    Navigation.findNavController(view).navigate(R.id.editBooksFragment,bundle);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) book.getId();
                    Snackbar.make(view, R.string.delete_book, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    dataManager.deleteBook(book);
                    mdata.remove(book);
                    notifyDataSetChanged();
                }
            });
            expand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    expandableVisivility = !expandableVisivility;
                    if(expandableVisivility) {
                        expandableLayout.setVisibility(View.VISIBLE);
                        expand.setBackgroundResource(R.drawable.ic_baseline_expand_less_24);
                    }
                    else {
                        expandableLayout.setVisibility(View.GONE);
                        expand.setBackgroundResource(R.drawable.ic_baseline_expand_more_24);
                    }
                }
            });
        }
    }
}
