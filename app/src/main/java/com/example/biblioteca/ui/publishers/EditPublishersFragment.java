package com.example.biblioteca.ui.publishers;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.biblioteca.R;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Author;
import com.example.biblioteca.data.models.Publisher;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

public class EditPublishersFragment extends Fragment {

    private EditPublishersViewModel mViewModel;

    public static EditPublishersFragment newInstance() {
        return new EditPublishersFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_publishers_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(EditPublishersViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View view) {
        //Getting data from bundle
        int id = 0;
        if(requireArguments() != null)
            id = (int) requireArguments().getFloat("id");
        Log.i("argument", String.valueOf(id));
        //Getting the data manager and the list of books
        final DataManager dataManager = new DataManager(requireActivity());

        //Cancel and save buttons
        Button cancelButtonPublisher = requireActivity().findViewById(R.id.cancel_button_publisher);
        Button saveButtonPublisher = requireActivity().findViewById(R.id.accept_button_publisher);
        //Debug text field
        TextView debug = requireActivity().findViewById(R.id.textView2);

        //Declare Data ModelTo fill
        Publisher publisher = dataManager.getPublisher(id);

        //Text Fields
        TextInputLayout textFieldTitle = requireActivity().findViewById(R.id.textFieldNamePublisher);

        if (id > 0 && publisher != null) {

            debug.setText(String.valueOf(id));
            //Text views
            Objects.requireNonNull(textFieldTitle.getEditText()).setText(publisher.getName());
        }

        cancelButtonPublisher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.nav_publishers);
            }
        });

        int finalId = id;
        saveButtonPublisher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Objects.requireNonNull(textFieldTitle.getEditText()).setError(null);
                if (textFieldTitle.getEditText().getText().toString().equals("")) {
                    Snackbar.make(view, R.string.must_fill_all_fields, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    return;
                }
                Publisher publisher = new Publisher(finalId,textFieldTitle.getEditText().getText().toString());
                if (finalId > 0)
                    dataManager.editPublisher(publisher);
                else
                    dataManager.addPublisher(publisher);
                Navigation.findNavController(view).navigate(R.id.nav_publishers);
            }
        });
    }

}