package com.example.biblioteca.ui.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biblioteca.R;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Author;
import com.example.biblioteca.data.models.Copy;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ListAdapterAuthors extends RecyclerView.Adapter<ListAdapterAuthors.ViewHolder> {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Author> mdata;
    //Recycler constructor
    public ListAdapterAuthors(List<Author> itemList, Context context){
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.mdata = itemList;
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    @Override
    public ListAdapterAuthors.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = layoutInflater.inflate(R.layout.list_element_author,null);
        return new ListAdapterAuthors.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final ListAdapterAuthors.ViewHolder holder, final int position) {
        holder.bindData(mdata.get(position));
    }

    public void setItems(List<Author> items) {
        mdata = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        //Declaring elements
        ImageView avatar;
        TextView primaryText; //Author
        Button edit;
        Button delete;

        ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.cardAvatar);
            primaryText = itemView.findViewById(R.id.cardMainText);
            edit = itemView.findViewById(R.id.linearLayoutCardViewButtonEdit);
            delete = itemView.findViewById(R.id.linearLayoutCardViewButtonDelete);
        }

        //Get The Data
        public void bindData(@NotNull final Author author) {
            //Getting data manager
            DataManager dataManager = new DataManager(context);

            //Printing data
            primaryText.setText(author.getName());

            //Buttons
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) author.getId();
                    Snackbar.make(view, R.string.edit_author, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    Bundle bundle = new Bundle(1);
                    bundle.putFloat("id",id);
                    Navigation.findNavController(view).navigate(R.id.editAuthorsFragment,bundle);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) author.getId();
                    try {
                        dataManager.deleteAuthor(author);
                        mdata.remove(author);
                        notifyDataSetChanged();
                        Snackbar.make(view, R.string.delete_author, Snackbar.LENGTH_LONG)
                                .setAction(R.string.action, null).show();
                    } catch (Exception e) {
                       Log.e(String.valueOf(R.string.fk),e.getMessage());
                       Snackbar.make(view, R.string.delete_author_error, Snackbar.LENGTH_LONG)
                                .setAction(R.string.action, null).show();
                    }
                }
            });
        }
    }
}
