package com.example.biblioteca.ui.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biblioteca.R;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Person;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class ListAdapterPersons extends RecyclerView.Adapter<ListAdapterPersons.ViewHolder> {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Person> mdata;
    public ListAdapterPersons(List<Person> itemList, Context context){
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.mdata = itemList;
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    @Override
    public ListAdapterPersons.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = layoutInflater.inflate(R.layout.list_element_person,null);
        return new ListAdapterPersons.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final ListAdapterPersons.ViewHolder holder, final int position) {
        holder.bindData(mdata.get(position));
    }

    public void setItems(List<Person> items) {
        mdata = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView avatar;
        TextView primaryText;
        TextView secondaryText;
        Button edit;
        Button delete;
        Button expand;
        TextView emailText;
        TextView stateText;
        TextView identificationText;
        TextView birthDateText;
        TextView registerDateText;
        LinearLayout expandableLayout;
        Boolean expandableVisivility = false;

        ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.cardAvatar);
            primaryText = itemView.findViewById(R.id.cardMainText);
            secondaryText = itemView.findViewById(R.id.cardSecondaryText);
            edit = itemView.findViewById(R.id.linearLayoutCardViewButtonEdit);
            delete = itemView.findViewById(R.id.linearLayoutCardViewButtonDelete);
            expand = itemView.findViewById(R.id.linearLayoutCardViewButtonExpand);
            //Expandable items
            emailText = itemView.findViewById(R.id.cardEmail);
            stateText = itemView.findViewById(R.id.cardState);
            identificationText = itemView.findViewById(R.id.cardIdentification);
            birthDateText = itemView.findViewById(R.id.cardBirthDate);
            registerDateText = itemView.findViewById(R.id.cardRegisterDate);
            expandableLayout = itemView.findViewById(R.id.expandablePersonLayout);
        }

        //Get The Person
        public void bindData(@NotNull final Person p) {
            DataManager dataManager = new DataManager(context);
            Locale spanish = new Locale("es", "ES");
            SimpleDateFormat simpleFormat = new SimpleDateFormat("MM/dd/yyyy", spanish);
            primaryText.setText(String.format("%s %s %s", p.getName(), p.getLastName1(), p.getLastName2()));
            secondaryText.setText(p.getPhone());
            emailText.setText(p.getEmail());
            stateText.setText(dataManager.getPersonState(p.getIdState()));
            identificationText.setText(p.getClaveElector());
            birthDateText.setText(simpleFormat.format(p.getBirthDate()));
            registerDateText.setText(simpleFormat.format(p.getRegisterDate()));
            //Buttons
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) p.getId();
                    Snackbar.make(view, R.string.edit_user, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    Bundle bundle = new Bundle(1);
                    bundle.putFloat("id",id);
                    //Navigation.findNavController(view).navigate(R.id.personsEdit);
                    Navigation.findNavController(view).navigate(R.id.personsEdit,bundle);
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) p.getId();
                    Snackbar.make(view, R.string.delete_user, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    dataManager.deletePerson(p);
                    mdata.remove(p);
                    notifyDataSetChanged();
                }
            });
            expand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    expandableVisivility = !expandableVisivility;
                    if(expandableVisivility) {
                        expandableLayout.setVisibility(View.VISIBLE);
                        expand.setBackgroundResource(R.drawable.ic_baseline_expand_less_24);
                    }
                    else {
                        expandableLayout.setVisibility(View.GONE);
                        expand.setBackgroundResource(R.drawable.ic_baseline_expand_more_24);
                    }
                }
            });
        }
    }
}
