package com.example.biblioteca.ui.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biblioteca.R;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Author;
import com.example.biblioteca.data.models.Publisher;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ListAdapterPublishers extends RecyclerView.Adapter<ListAdapterPublishers.ViewHolder> {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Publisher> mdata;
    //Recycler constructor
    public ListAdapterPublishers(List<Publisher> itemList, Context context){
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.mdata = itemList;
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    @Override
    public ListAdapterPublishers.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = layoutInflater.inflate(R.layout.list_element_publisher,null);
        return new ListAdapterPublishers.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final ListAdapterPublishers.ViewHolder holder, final int position) {
        holder.bindData(mdata.get(position));
    }

    public void setItems(List<Publisher> items) {
        mdata = items;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        //Declaring elements
        ImageView avatar;
        TextView primaryText; //Author
        Button edit;
        Button delete;

        ViewHolder(View itemView) {
            super(itemView);
            avatar = itemView.findViewById(R.id.cardAvatar);
            primaryText = itemView.findViewById(R.id.cardMainText);
            edit = itemView.findViewById(R.id.linearLayoutCardViewButtonEdit);
            delete = itemView.findViewById(R.id.linearLayoutCardViewButtonDelete);
        }

        //Get The Data
        public void bindData(@NotNull final Publisher publisher) {
            //Getting data manager
            DataManager dataManager = new DataManager(context);

            //Printing data
            primaryText.setText(publisher.getName());

            //Buttons
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) publisher.getId();
                    Snackbar.make(view, R.string.edit_publisher, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    Bundle bundle = new Bundle(1);
                    bundle.putFloat("id",id);
                    Navigation.findNavController(view).navigate(R.id.editPublishersFragment,bundle);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float id = (float) publisher.getId();
                    Snackbar.make(view, R.string.delete_publisher, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    dataManager.deletePublisher(publisher);
                    mdata.remove(publisher);
                    notifyDataSetChanged();
                }
            });
        }
    }
}
