package com.example.biblioteca.ui.penalties;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PenaltiesViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public PenaltiesViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is penalties fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}