package com.example.biblioteca.ui.loans;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class LoansViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public LoansViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is loans fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}