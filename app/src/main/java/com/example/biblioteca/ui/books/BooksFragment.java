package com.example.biblioteca.ui.books;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biblioteca.R;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Book;
import com.example.biblioteca.ui.adapters.ListAdapterBooks;
import com.example.biblioteca.ui.adapters.ListAdapterPersons;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import static com.example.biblioteca.R.string.add_new_user;

public class BooksFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        BooksViewModel bookViewModel = new ViewModelProvider(this).get(BooksViewModel.class);
        //Return the view
        return inflater.inflate(R.layout.fragment_books, container, false);
    }

    private void init(@NotNull View view) {
        final DataManager dataManager = new DataManager(requireActivity());
        ArrayList<Book> books = dataManager.getAllBooks();
        ListAdapterBooks listAdapterBooks = new ListAdapterBooks(books,getActivity());
        RecyclerView recyclerView = view.findViewById(R.id.booksRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(listAdapterBooks);


        FloatingActionButton fab = view.findViewById(R.id.booksFab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //int id = 0;
                Snackbar.make(view, R.string.add_new_book, Snackbar.LENGTH_LONG)
                        .setAction(R.string.action, null).show();
                //Bundle bundle = new Bundle(1);
                //bundle.putFloat("id",id);
                Navigation.findNavController(view).navigate(R.id.editBooksFragment);
                //Navigation.findNavController(view).navigate(R.id.personsEdit,bundle);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);
    }
}