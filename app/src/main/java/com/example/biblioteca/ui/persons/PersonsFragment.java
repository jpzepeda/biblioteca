package com.example.biblioteca.ui.persons;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biblioteca.R;
import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Person;
import com.example.biblioteca.ui.adapters.ListAdapterPersons;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import static com.example.biblioteca.R.string.add_new_user;

public class PersonsFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        PersonsViewModel personsViewModel = new ViewModelProvider(this).get(PersonsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_persons, container, false);
        return root;
    }

    private void init(@NotNull View view) {
        final DataManager dataManager = new DataManager(requireActivity());
        ArrayList<Person> persons = dataManager.getAllPersons();
        ListAdapterPersons listAdapterPersons = new ListAdapterPersons(persons,getActivity());
        RecyclerView recyclerView = view.findViewById(R.id.personsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(listAdapterPersons);


        FloatingActionButton fab = view.findViewById(R.id.personsFab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //int id = 0;
                Snackbar.make(view, add_new_user, Snackbar.LENGTH_LONG)
                        .setAction(R.string.action, null).show();
                //Bundle bundle = new Bundle(1);
                //bundle.putFloat("id",id);
                Navigation.findNavController(view).navigate(R.id.personsEdit);
                //Navigation.findNavController(view).navigate(R.id.personsEdit,bundle);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);
    }
}