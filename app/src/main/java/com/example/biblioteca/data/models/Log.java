package com.example.biblioteca.data.models;

public class Log {
    private final int id;
    private int type;
    private String tableName;
    private int pk;

    public int getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public Log(int id, int type, String tableName, int pk) {
        this.id = id;
        this.type = type;
        this.tableName = tableName;
        this.pk = pk;
    }
}
