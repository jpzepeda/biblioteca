package com.example.biblioteca.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.biblioteca.data.models.Author;
import com.example.biblioteca.data.models.Book;
import com.example.biblioteca.data.models.Copy;
import com.example.biblioteca.data.models.Loan;
import com.example.biblioteca.data.models.Penalty;
import com.example.biblioteca.data.models.Person;
import com.example.biblioteca.data.models.Publisher;
import com.google.android.material.textfield.TextInputLayout;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Objects;

public class DataManager {
    /*
        Next we have a private static final strings for
        each row/table that we need to refer to just
        inside this class
    */
    //Persons table
    private static final String TABLE_PERSONS = "personas";
    private static final String TABLE_PERSONS_NAME = "nombre";
    private static final String TABLE_PERSONS_LN1 = "apellido_paterno";
    private static final String TABLE_PERSONS_LN2 = "apellido_materno";
    private static final String TABLE_PERSONS_IDENTIFICATION_NUMBER = "clave_elector";
    private static final String TABLE_PERSONS_STATE_ID = "id_estados";
    private static final String TABLE_PERSONS_BIRTDATE = "fecha_nacimiento";
    private static final String TABLE_PERSONS_PHONE = "telefono";
    private static final String TABLE_PERSONS_EMAIL = "email";

    private static final String DB_NAME = "biblioteca.db";

    //0 Create 1 Read 2 Update 3 Delete
    //0 in id means new in create, 0 en read means all table
    private static final int CREATE = 0;
    private static final int READ = 1;
    private static final int UPDATE = 2;
    private static final int DELETE = 3;
    /*
        Next we have a public static final string for
        each row/table that we need to refer to both
        inside and outside this class
    */
    private static final int DB_VERSION = 3;
    private static final String TABLE_AUTHORS = "autores";
    private static final String TABLE_PUBLISHERS = "editorial";
    private static final String TABLE_BOOKS = "libros";
    private static final String TABLE_COPIES = "ejemplares";
    private static final String TABLE_LOANS = "prestamos";
    private static final String TABLE_PENALTIES = "penalizacion";
    private static final String STATE_NAME = "nombre_estado";
    private static final String TABLE_PERSONS_STATE = "estados";
    private static final String STATE_DESCRIPTION = "descripcion";
    private static final String TABLE_COPY_STATE = "estado_de_ejemplar";
    private static final String LANGUAGE_ISO = "iso_code";
    private static final String TABLE_LANGUAGES = "idiomas";
    private static final String STATE_ID = "id_estados";
    private static final String COPY_STATE_ID = "id_estado_fisico";
    private static final String LANGUAGE_ID = "id_idioma";
    private static final String TABLE_PERSONS_ID = "id_persona";
    private static final String TABLE_BOOKS_ID = "id_libro";
    private static final String TABLE_BOOKS_TITLE = "titulo";
    private static final String TABLE_BOOKS_ID_PUBLISHER = "id_editorial";
    private static final String TABLE_BOOKS_ID_LANGUAGE = "id_idioma";
    private static final String TABLE_BOOKS_N_PAGES = "n_paginas";
    private static final String TABLE_BOOKS_ENTER_DATE = "fecha_ingreso";
    private static final String TABLE_BOOKS_RARE = "raresa";
    private static final String TABLE_BOOKS_ID_AUTHOR = "id_autor";
    private static final String TABLE_BOOKS_ISBN = "isbn";
    private static final String TABLE_AUTHORS_NAME = "nombre_autor";
    private static final String TABLE_PUBLISHERS_NAME = "nombre";
    private static final String TABLE_PUBLISHERS_ID = "id_editorial";
    private static final String TABLE_COPIES_ID = "id_ejemplar";
    private static final String TABLE_AUTHORS_ID = "id_autor";
    private static final String TABLE_PERSONS_PASS = "pass";
    private static final String TABLE_LOGGIN = "Loggin";
    private static final String TABLE_LOGGIN_ID = "id";
    private static final String TABLE_LOGGIN_TYPE = "tipo";
    private static final String TABLE_LOGGIN_TABLE_NAME = "tabla";
    private static final String TABLE_LOGGIN_TABLE_ID = "tabla_id";
    private static String DB_PATH;
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    // This is the actual database
    private SQLiteDatabase db;

    public DataManager(Context context) {
        // Create an instance of our internal CustomSQLiteOpenHelper

        DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        String FULL_PATH = DB_PATH + DB_NAME;
        File file = new File(FULL_PATH);
        if(!file.exists()) {
            CustomSQLiteOpenHelper helper = new
                    CustomSQLiteOpenHelper(context);

            db = helper.getWritableDatabase();
            this.copyDatabase(context);
        }
        Log.i("Info", String.format("DB in: %s", FULL_PATH));

        CustomSQLiteOpenHelper helper = new
                CustomSQLiteOpenHelper(context);
/*

        try {
            helper.checkDatabase();
        }catch (Exception e){
            Log.i("Error check db e: ", e.getMessage());
        }
*/
/*
        try {
            helper.copyDatabase();
        }catch (Exception e){
            Log.i("Error copy db e: ", e.getMessage());
        }
*/


        // Get a writable database
        db = helper.getWritableDatabase();
        db.execSQL("PRAGMA foreign_keys=ON;");
/*
        String query = "select * from autores";
        Cursor cursor = db.rawQuery(query, null);
        Log.i("Count", String.valueOf(cursor.getCount()));

        getAllPersons();
        getAllAuthors();
        getAllPublishers();
        getAllBooks();
        getAllCopies();
        getAllLoans();
        getAllPenalties();

        Log.i("State", getPersonState(4));
        Log.i("Physical State", getCopiState(4));
        Log.i("Language", getLanguage(4));
        Log.i("Iso Code:", getIsoCode(4));
*/
    }

    public void copyDatabase(Context context) {
        try {
            Log.i("DB Ct", "Starting to copy database");
            for (String asset : context.getAssets().list("")) {
                Log.i("DB Ct", asset);
            }
            InputStream inputStream = context.getAssets().open(DB_NAME);
            Log.i("DB Size", String.valueOf(inputStream.available()));


            String outFileName = DB_PATH + DB_NAME;
            Log.i("DB Ct outfile", outFileName);

            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];

            int lenght;
            int total=0;


            while ((lenght = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, lenght);
                total += lenght;
            }
            inputStream.close();
            outputStream.flush();
            outputStream.close();
            Log.i("Escrito: ", String.valueOf(total));

        } catch (Exception e) {
            Log.i("Error copy db: ", e.getMessage());
        }
    }

    //0 Create 1 Read 2 Update 3 Delete
    //0 in id means new in create, 0 en read means all table
    //Persons
    public boolean addPerson(Person person) {
        Log.i("Estado","Añadiendo");
        Log.i("Estado",getPersonState(person.getIdState()));
        String query = String.format("insert into %s(%s,%s,%s,%s,%s,%s,%s,%s,%s) values (?,?,?,?,?,?,?,?,?);", TABLE_PERSONS, TABLE_PERSONS_NAME, TABLE_PERSONS_LN1, TABLE_PERSONS_LN2, TABLE_PERSONS_IDENTIFICATION_NUMBER, TABLE_PERSONS_STATE_ID, TABLE_PERSONS_BIRTDATE, TABLE_PERSONS_PHONE, TABLE_PERSONS_EMAIL, TABLE_PERSONS_PASS);
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments = new String[]{person.getName(),person.getLastName1(),person.getLastName2(),person.getClaveElector(), String.valueOf(person.getIdState()),format.format(person.getBirthDate()),person.getPhone(),person.getEmail(),person.getHashedPassword()};
        String[] arguments2 = new String[]{String.valueOf(CREATE),TABLE_PERSONS, String.valueOf(0)};
        db.execSQL("PRAGMA foreign_keys=OFF;");
        db.execSQL(query,arguments);
        db.execSQL(query2,arguments2);
        db.execSQL("PRAGMA foreign_keys=ON;");
        return true;
    }

    public void close() {
        db.close();
    }

    public boolean deletePerson(@NotNull Person person) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(DELETE),TABLE_PERSONS, String.valueOf(person.getId())};
        db.delete(TABLE_PERSONS,TABLE_PERSONS_ID+" = ?",new String[]{String.valueOf(person.getId())});
        db.execSQL(query2,arguments2);
        db.close();
        return true;
    }

    public boolean deleteBook(@NotNull Book book) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(DELETE),TABLE_BOOKS, String.valueOf(book.getId())};
        db.delete(TABLE_BOOKS, String.format("%s = ?", TABLE_BOOKS_ID),new String[]{String.valueOf(book.getId())});
        db.execSQL(query2,arguments2);
        db.close();
        return true;
    }

    public boolean editBook(@NotNull Book book) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(UPDATE),TABLE_BOOKS, String.valueOf(book.getId())};
        Log.i("Editting book: ",book.getTitle());
        String query = String.format("update %s set %s=?,%s=?,%s=?,%s=?,%s=?,%s=?,%s=? where %s = ?", TABLE_BOOKS, TABLE_BOOKS_TITLE, TABLE_BOOKS_ID_PUBLISHER, TABLE_BOOKS_ID_LANGUAGE, TABLE_BOOKS_N_PAGES, TABLE_BOOKS_ISBN, TABLE_BOOKS_RARE, TABLE_BOOKS_ID_AUTHOR, TABLE_BOOKS_ID);
        String[] arguments = new String[]{
                book.getTitle(),
                String.valueOf(book.getIdPublisher()),
                String.valueOf(book.getIdLanguage()),
                String.valueOf(book.getPages()),
                String.valueOf(book.getIsbn()),
                String.valueOf(book.getRare()),
                String.valueOf(book.getIdAuthor()),
                String.valueOf(book.getId()),
        };
        db.execSQL("PRAGMA foreign_keys=OFF;");
        db.execSQL(query,arguments);
        db.execSQL(query2,arguments2);
        db.execSQL("PRAGMA foreign_keys=ON;");
        return true;
    }

    public Person getPerson(int id) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_PERSONS, String.valueOf(id)};
        Person person;
        String query = "select * from " + TABLE_PERSONS + " where " + TABLE_PERSONS_ID + " = " + id;
        Cursor cursor = db.rawQuery(query, null);
        // Move to first row
        cursor.moveToFirst();
            try {
                person = new Person(cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getInt(5),
                        format.parse(cursor.getString(6)),
                        format.parse(cursor.getString(7)),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10));
                Log.i("Person: ", person.getName());
                Log.i("Person email: ", person.getName().toString());
                db.execSQL(query2,arguments2);
                return person;
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e("Error: ", e.getMessage());
                return null;
            }

    }

    public ArrayList<Person> getAllPersons() {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_PERSONS, String.valueOf(0)};
        ArrayList<Person> persons = new ArrayList<Person>();
        String query = "select * from " + TABLE_PERSONS;
        Cursor cursor = db.rawQuery(query, null);
        // Move to first row
        cursor.moveToFirst();
        for (String column : cursor.getColumnNames()
        ) {
            Log.i("Columns: ", column);
        }
        Log.i("Number: ", String.valueOf(cursor.getCount()));
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            Log.i("Index: ", String.valueOf(cursor.getString(0)));
            try {
                Person person;
                person = new Person(cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getInt(5),
                        format.parse(cursor.getString(6)),
                        format.parse(cursor.getString(7)),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10));
                Log.i("Person: ", person.toString());
                Log.i("Person email: ", person.getName().toString());
                persons.add(person);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e("Error: ", e.getMessage());
            }
            cursor.moveToNext();
        }


        Log.i("Persons: ", String.valueOf(persons.size()));
        db.execSQL(query2,arguments2);
        return persons;
    }


    public String getPersonState (int id){
        String query = "select "+STATE_NAME+" from " + TABLE_PERSONS_STATE + " where " + STATE_ID + " = " + id;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getCopiState (int id){
        String query = String.format("select %s from %s where %s = %d", STATE_DESCRIPTION, TABLE_COPY_STATE, COPY_STATE_ID, id);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getLanguage (int id){
        String query = String.format("select %s from %s where %s = ?", STATE_DESCRIPTION, TABLE_LANGUAGES, LANGUAGE_ID);
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getTitle (int id){
        String query = String.format("select %s from %s where %s = ?", TABLE_BOOKS_TITLE, TABLE_BOOKS, TABLE_BOOKS_ID);
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getIsoCode (int id){
        String query = "select "+LANGUAGE_ISO+" from " + TABLE_LANGUAGES + " where " + LANGUAGE_ID  + " = " + id;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    //Author name by Id
    public String getAuthorName (int id){
        String query = String.format("select %s from %s where %s = ?", TABLE_AUTHORS_NAME, TABLE_AUTHORS, TABLE_BOOKS_ID_AUTHOR);
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        if(cursor.getString(0) != null)
            return cursor.getString(0);
        else
            return "Not Founded";
    }

    //Publisher name by Id
    public String getPublisherName (int id){
        String query = String.format("select %s from %s where %s = ?", TABLE_PUBLISHERS_NAME, TABLE_PUBLISHERS, TABLE_PUBLISHERS_ID);
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    private void deleteElement (String tableName,String idName, int id){
        String query = "delete from " + tableName + " where " + idName  + " = " + id;
        Cursor cursor = db.rawQuery(query, null);
    }

    public ArrayList<Loan> getAllLoans() {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_LOANS, String.valueOf(0)};
        ArrayList<Loan> loans = new ArrayList<Loan>();
        String query = "select * from " + TABLE_LOANS;
        Cursor cursor = db.rawQuery(query, null);
        // Move to first row
        cursor.moveToFirst();
        for (String column : cursor.getColumnNames()
        ) {
            Log.i("Columns: ", column);
        }
        Log.i("Number: ", String.valueOf(cursor.getCount()));
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            Log.i("Index: ", String.valueOf(cursor.getString(0)));
            try {
                Loan loan;
                loan = new Loan(cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getInt(2),
                        format.parse(cursor.getString(3)),
                        format.parse(cursor.getString(4)));
                Log.i("Loan: ", loan.toString());
                Log.i("Loan date", loan.getOutDate().toString());
                loans.add(loan);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e("Error: ", e.getMessage());
            }
            cursor.moveToNext();
        }


        Log.i("Loans: ", String.valueOf(loans.size()));
        db.execSQL(query2,arguments2);
        return loans;
    }

    public ArrayList<Copy> getAllCopies() {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_COPIES, String.valueOf(0)};
        ArrayList<Copy> copies = new ArrayList<Copy>();
        String query = "select * from " + TABLE_COPIES;
        Cursor cursor = db.rawQuery(query, null);
        // Move to first row
        cursor.moveToFirst();
        for (String column : cursor.getColumnNames()
        ) {
            Log.i("Columns: ", column);
        }
        Log.i("Number: ", String.valueOf(cursor.getCount()));
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            Log.i("Index: ", String.valueOf(cursor.getString(0)));
                Copy copy;
                copy = new Copy(cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getInt(2),
                        cursor.getInt(3));
                Log.i("Copy: ", copy.toString());
                Log.i("Copy: ", String.valueOf(copy.getIdState()));
                copies.add(copy);
            cursor.moveToNext();
        }


        Log.i("Copies: ", String.valueOf(copies.size()));
        db.execSQL(query2,arguments2);
        return copies;
    }

    public ArrayList<com.example.biblioteca.data.models.Log> getAllLogs() {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_LOGGIN, String.valueOf(0)};
        ArrayList<com.example.biblioteca.data.models.Log> logs = new ArrayList<com.example.biblioteca.data.models.Log>();
        String query = "select * from " + TABLE_LOGGIN;
        Cursor cursor = db.rawQuery(query, null);
        // Move to first row
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            com.example.biblioteca.data.models.Log log;
            log = new com.example.biblioteca.data.models.Log(cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getString(2),
                    cursor.getInt(3));
            logs.add(log);
            cursor.moveToNext();
        }

        db.execSQL(query2,arguments2);
        return logs;
    }

    public ArrayList<Author> getAllAuthors() {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_AUTHORS, String.valueOf(0)};
        ArrayList<Author> authors = new ArrayList<Author>();
        String query = "select * from " + TABLE_AUTHORS;
        Cursor cursor = db.rawQuery(query, null);
        // Move to first row
        cursor.moveToFirst();
        for (String column : cursor.getColumnNames()
        ) {
            Log.i("Columns: ", column);
        }
        Log.i("Number: ", String.valueOf(cursor.getCount()));
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            Log.i("Index: ", String.valueOf(cursor.getString(0)));
                Author author;
                author = new Author(cursor.getInt(0),
                        cursor.getString(1));
                Log.i("Author: ", author.getName());
                authors.add(author);
            cursor.moveToNext();
        }


        Log.i("Authors: ", String.valueOf(authors.size()));
        db.execSQL(query2,arguments2);
        return authors;
    }

    public ArrayList<Penalty> getAllPenalties() {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_PENALTIES, String.valueOf(0)};
        ArrayList<Penalty> penalties = new ArrayList<Penalty>();
        String query = "select * from " + TABLE_PENALTIES;
        Cursor cursor = db.rawQuery(query, null);
        // Move to first row
        cursor.moveToFirst();
        for (String column : cursor.getColumnNames()
        ) {
            Log.i("Columns: ", column);
        }
        Log.i("Number: ", String.valueOf(cursor.getCount()));
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            Log.i("Index: ", String.valueOf(cursor.getString(0)));
            Penalty penalty;
            penalty = new Penalty(cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getInt(2));
            Log.i("Penalty donation: ", String.valueOf(penalty.getDonation()));
            penalties.add(penalty);
            cursor.moveToNext();
        }


        Log.i("Penalties: ", String.valueOf(penalties.size()));
        db.execSQL(query2,arguments2);
        return penalties;
    }

    public ArrayList<Publisher> getAllPublishers() {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_PUBLISHERS, String.valueOf(0)};
        ArrayList<Publisher> publishers = new ArrayList<Publisher>();
        String query = "select * from " + TABLE_PUBLISHERS;
        Cursor cursor = db.rawQuery(query, null);
        // Move to first row
        cursor.moveToFirst();
        for (String column : cursor.getColumnNames()
        ) {
            Log.i("Columns: ", column);
        }
        Log.i("Number: ", String.valueOf(cursor.getCount()));
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            Log.i("Index: ", String.valueOf(cursor.getString(0)));
            Publisher publisher;
            publisher = new Publisher(cursor.getInt(0),
                    cursor.getString(1));
            Log.i("Publisher: ", publisher.getName());
            publishers.add(publisher);
            cursor.moveToNext();
        }


        Log.i("Publishers: ", String.valueOf(publishers.size()));
        db.execSQL(query2,arguments2);
        return publishers;
    }

    public boolean editPerson(@NotNull Person person) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(UPDATE),TABLE_PERSONS, String.valueOf(person.getId())};
        Log.i("Estado","Ediatando");
        Log.i("Estado",getPersonState(person.getIdState()));
        String query = String.format("update %s set %s=?,%s=?,%s=?,%s=?,%s=?,%s=?,%s=?,%s=?,%s=? where %s = ?", TABLE_PERSONS, TABLE_PERSONS_NAME, TABLE_PERSONS_LN1, TABLE_PERSONS_LN2, TABLE_PERSONS_IDENTIFICATION_NUMBER, TABLE_PERSONS_STATE_ID, TABLE_PERSONS_BIRTDATE, TABLE_PERSONS_PHONE, TABLE_PERSONS_EMAIL, TABLE_PERSONS_PASS, TABLE_PERSONS_ID);
        //Format Date to SQL Format
        String[] arguments = new String[]{person.getName(),person.getLastName1(),person.getLastName2(),person.getClaveElector(), String.valueOf(person.getIdState()),format.format(person.getBirthDate()),person.getPhone(),person.getEmail(),String.valueOf(person.getHashedPassword()),String.valueOf(person.getId())};
        db.execSQL("PRAGMA foreign_keys=OFF;");
        db.execSQL(query,arguments);
        db.execSQL(query2,arguments2);
        db.execSQL("PRAGMA foreign_keys=ON;");
        return true;
    }

    public boolean addBook(@NotNull Book book) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(CREATE),TABLE_BOOKS, String.valueOf(book.getId())};
        Log.i("Creando libro: ",book.getTitle());
        String query = String.format("insert into %s(%s,%s,%s,%s,%s,%s,%s) values (?,?,?,?,?,?,?);", TABLE_BOOKS, TABLE_BOOKS_TITLE, TABLE_BOOKS_ID_PUBLISHER, TABLE_BOOKS_ID_LANGUAGE, TABLE_BOOKS_N_PAGES, TABLE_BOOKS_ISBN, TABLE_BOOKS_RARE, TABLE_BOOKS_ID_AUTHOR);
        //Format Arguments array
        String[] arguments = new String[]{
                book.getTitle(),
                String.valueOf(book.getIdPublisher()),
                String.valueOf(book.getIdLanguage()),
                String.valueOf(book.getPages()),
                String.valueOf(book.getIsbn()),
                String.valueOf(book.getRare()),
                String.valueOf(book.getIdAuthor()),
        };
        db.execSQL(query,arguments);
        db.execSQL(query2,arguments2);
        return true;
    }

    public Book getBook(int id) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_BOOKS, String.valueOf(id)};
        if(id > 0) {
            Book book;
            String query = String.format("select * from %s where %s = ?", TABLE_BOOKS, TABLE_BOOKS_ID);
            Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
            // Move to first row
            cursor.moveToFirst();
            try {
                book = new Book(cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getInt(2),
                        cursor.getInt(3),
                        cursor.getInt(4),
                        cursor.getInt(5),
                        format.parse(cursor.getString(6)),
                        cursor.getInt(7),
                        cursor.getInt(8)
                );
                Log.i("Booktuki: ", book.getTitle());
                db.execSQL(query2,arguments2);
                return book;
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e("Error: ", e.getMessage());
                return null;
            }
        }else
            return null;
    }
    
    public ArrayList<Book> getAllBooks() {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_BOOKS, String.valueOf(0)};
        ArrayList<Book> books = new ArrayList<Book>();
        String query = "select * from " + TABLE_BOOKS;
        Cursor cursor = db.rawQuery(query, null);
        // Move to first row
        cursor.moveToFirst();
        for (String column : cursor.getColumnNames()
        ) {
            Log.i("Columns: ", column);
        }
        Log.i("Number: ", String.valueOf(cursor.getCount()));
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            Log.i("Index: ", String.valueOf(cursor.getString(0)));
            try {
            Book book = null;
            Log.i("Columnas", String.valueOf(cursor.getColumnCount()));
                for (String culumn: cursor.getColumnNames()) {
                    Log.i("Columna:", String.valueOf(culumn));
                }
                Log.i("Fecha:", Objects.requireNonNull(format.parse(cursor.getString(6))).toString());
                book = new Book(cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getInt(2),
                    cursor.getInt(3),
                    cursor.getInt(4),
                    cursor.getInt(5),
                    format.parse(cursor.getString(6)),
                    cursor.getInt(7),
                    cursor.getInt(8)
                    );
                        Log.i("Title: ", book.getTitle());
                Log.i("Id: ", String.valueOf(book.getId()));
                Log.i("Editorial: ", String.valueOf(book.getIdPublisher()));
                Log.i("Idioma: ", String.valueOf(book.getIdLanguage()));
                Log.i("Pages: ", String.valueOf(book.getPages()));
                Log.i("ISBN: ", String.valueOf(book.getIsbn()));
                Log.i("Date: ", format.format(book.getEntryDate()));
                Log.i("Rare: ", String.valueOf(book.getRare()));
                Log.i("Author: ", String.valueOf(book.getIdAuthor()));
            books.add(book);
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e("Error: ", e.getMessage());
            }
            cursor.moveToNext();
        }


        Log.i("Books brr: ", String.valueOf(books.size()));
        db.execSQL(query2,arguments2);
        return books;
    }

    public String[] getAllLanguages() {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_LANGUAGES, String.valueOf(0)};
        String query = String.format("select %s from %s", STATE_DESCRIPTION, TABLE_LANGUAGES);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        String[] languages = new String[cursor.getCount()];

        for (int i = 0; i < languages.length; i++) {
           languages[i]  = cursor.getString(0);
           cursor.moveToNext();
           Log.i("Language",languages[i]);
        }

        db.execSQL(query2,arguments2);
        return languages;
    }

    public String[] getAllAuthorsAsString() {
        ArrayList<Author> authors = this.getAllAuthors();
        String[] authorsAsString = new String[authors.size()];
        for (int i = 0; i < authorsAsString.length; i++)
            authorsAsString[i] = authors.get(i).getName();
       return authorsAsString;
    }

    public String[] getAllTitlesAsString() {
        ArrayList<Book> books = this.getAllBooks();
        String[] titlesAsString = new String[books.size()];
        for (int i = 0; i < titlesAsString.length; i++)
            titlesAsString[i] = books.get(i).getTitle();
        return titlesAsString;
    }

    public String[] getAllPublishersAsString() {
        ArrayList<Publisher> publishers = this.getAllPublishers();
        String[] publishersAsString = new String[publishers.size()];
        for (int i = 0; i < publishersAsString.length; i++)
            publishersAsString[i] = publishers.get(i).getName();
        return publishersAsString;
    }

    public void deleteCopy(@NotNull Copy copy) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(DELETE),TABLE_COPIES, String.valueOf(copy.getId())};
        db.delete(TABLE_COPIES, String.format("%s = ?", TABLE_COPIES_ID),new String[]{String.valueOf(copy.getId())});
        db.execSQL(query2,arguments2);
        db.close();
    }

    public void deleteAuthor(@NotNull Author author) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(DELETE),TABLE_AUTHORS, String.valueOf(author.getId())};
        db.delete(TABLE_AUTHORS, String.format("%s = ?", TABLE_AUTHORS_ID),new String[]{String.valueOf(author.getId())});
        db.execSQL(query2,arguments2);
        db.close();
    }

    public void deletePublisher(Publisher publisher) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(DELETE),TABLE_PUBLISHERS, String.valueOf(publisher.getId())};
        db.delete(TABLE_PUBLISHERS, String.format("%s = ?", TABLE_PUBLISHERS_ID),new String[]{String.valueOf(publisher.getId())});
        db.execSQL(query2,arguments2);
        db.close();
    }

    public Author getAuthor(int id) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_AUTHORS, String.valueOf(id)};
        if(id > 0) {
            Author author;
            String query = String.format("select * from %s where %s = ?", TABLE_AUTHORS, TABLE_AUTHORS_ID);
            try (Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)})) {
                // Move to first row
                cursor.moveToFirst();
                author = new Author(cursor.getInt(0),
                        cursor.getString(1)
                );
                cursor.close();
                db.execSQL(query2,arguments2);
                return author;
            } catch (Exception e){
                Log.e("Cursor Exeption",e.getMessage());
                return null;
            }
        }else
            return null;
    }

    public boolean editAuthor(Author author) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(UPDATE),TABLE_AUTHORS, String.valueOf(author.getId())};
        String query = String.format("update %s set %s=? where %s = ?", TABLE_AUTHORS, TABLE_AUTHORS_NAME, TABLE_AUTHORS_ID);
        //Format Date to SQL Format
        String[] arguments = new String[]{author.getName(), String.valueOf(author.getId())};
        db.execSQL("PRAGMA foreign_keys=OFF;");
        db.execSQL(query, arguments);
        db.execSQL(query2,arguments2);
        db.execSQL("PRAGMA foreign_keys=ON;");
        return true;
    }

    public boolean addAuthor(Author author) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(CREATE),TABLE_AUTHORS, String.valueOf(0)};
        String query = String.format("insert into %s(%s) values (?);", TABLE_AUTHORS, TABLE_AUTHORS_NAME);
        //Format Date to SQL Format
        String[] arguments = new String[]{author.getName()};
        db.execSQL(query,arguments);
        db.execSQL(query2,arguments2);
        return true;
    }

    public Publisher getPublisher(int id) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(READ),TABLE_PUBLISHERS, String.valueOf(id)};
        if(id > 0) {
            Publisher publisher;
            String query = String.format("select * from %s where %s = ?", TABLE_PUBLISHERS, TABLE_PUBLISHERS_ID);
            try (Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)})) {
                // Move to first row
                cursor.moveToFirst();
                publisher = new Publisher(cursor.getInt(0),
                        cursor.getString(1)
                );
                cursor.close();
                db.execSQL(query2,arguments2);
                return publisher;
            } catch (Exception e){
                Log.e("Cursor Exeption",e.getMessage());
                return null;
            }
        }else
            return null;
    }

    public boolean editPublisher(Publisher publisher) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(UPDATE),TABLE_PUBLISHERS, String.valueOf(publisher.getId())};
        String query = String.format("update %s set %s=? where %s = ?", TABLE_PUBLISHERS, TABLE_PUBLISHERS_NAME, TABLE_PUBLISHERS_ID);
        //Format Date to SQL Format
        String[] arguments = new String[]{publisher.getName(), String.valueOf(publisher.getId())};
        db.execSQL("PRAGMA foreign_keys=OFF;");
        db.execSQL(query, arguments);
        db.execSQL(query2,arguments2);
        db.execSQL("PRAGMA foreign_keys=ON;");
        return true;
    }

    public boolean addPublisher(Publisher publisher) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        String[] arguments2 = new String[]{String.valueOf(CREATE),TABLE_PUBLISHERS, String.valueOf(0)};
        String query = String.format("insert into %s(%s) values (?);", TABLE_PUBLISHERS, TABLE_PUBLISHERS_NAME);
        //Format Date to SQL Format
        String[] arguments = new String[]{publisher.getName()};
        db.execSQL(query,arguments);
        db.execSQL(query2,arguments2);
        return true;
    }

    public Person getPerson(String email) {
        String query2 = String.format("insert into %s(%s,%s,%s) values (?,?,?);", TABLE_LOGGIN, TABLE_LOGGIN_TYPE, TABLE_LOGGIN_TABLE_NAME, TABLE_LOGGIN_TABLE_ID);
        Person person;
        String query = String.format("select * from %s where %s like '%s'", TABLE_PERSONS, TABLE_PERSONS_EMAIL, email);
        Cursor cursor = db.rawQuery(query, null);
        // Move to first row
        cursor.moveToFirst();
        try {
            person = new Person(cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getString(4),
                    cursor.getInt(5),
                    format.parse(cursor.getString(6)),
                    format.parse(cursor.getString(7)),
                    cursor.getString(8),
                    cursor.getString(9),
                    cursor.getString(10));
            Log.i("Person: ", person.getName());
            Log.i("Person hash: ", person.getHashedPassword());
            String[] arguments2 = new String[]{String.valueOf(READ),TABLE_PERSONS, String.valueOf(person.getId())};
            db.execSQL(query2,arguments2);
            return person;
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("Error: ", e.getMessage());
            return null;
        }

    }

    // This class is created when our DataManager is initialized
    private class CustomSQLiteOpenHelper extends SQLiteOpenHelper {
        Context context;

        public CustomSQLiteOpenHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
            this.context = context;
        }

        // This runs the first time the database is created

        //Read sqlite script
        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        // This method only runs when we increment DB_VERSION

        @Override
        public void onUpgrade(SQLiteDatabase db,
                              int oldVersion, int newVersion) {
            // Not needed in this app
            // but we must still override it

        }

        public void checkDatabase() {
            try {
                String path = DB_PATH + DB_NAME;

                SQLiteDatabase.openDatabase(path, null, 0);
                Log.i("DB Checket", "Correct check");


            } catch (Exception e) {
                Log.i("Error check db: ", e.getMessage());
            }

            this.getReadableDatabase();
            //CustomSQLiteOpenHelper.copyDatabase();
        }


        public void OpenDatabase() {
            String path = DB_PATH + DB_NAME;

            SQLiteDatabase.openDatabase(path, null, 0);

        }

    }

}
