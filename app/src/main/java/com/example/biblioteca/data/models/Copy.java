package com.example.biblioteca.data.models;

public class Copy {
    private final int id;
    private final int idBook;
    private int idState;
    private int edicion;
    //idState is on DataManager

    public Copy(int id, int idBook, int idState, int edicion) {
        this.id = id;
        this.idBook = idBook;
        this.idState = idState;
        this.edicion = edicion;
    }

    public int getId() {
        return id;
    }

    public int getIdBook() {
        return idBook;
    }

    public int getIdState() {
        return idState;
    }

    public void setIdState(int idState) {
        this.idState = idState;
    }

    public int getEdicion() {
        return edicion;
    }

    public void setEdicion(int edicion) {
        this.edicion = edicion;
    }
}
