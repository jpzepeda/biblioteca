package com.example.biblioteca.data.models;

public class Penalty {
    private final int id;
    private final int idCopy;
    private int donation;

    public Penalty(int id, int idCopy, int donation) {
        this.id = id;
        this.idCopy = idCopy;
        this.donation = donation;
    }

    public int getId() {
        return id;
    }

    public int getIdCopy() {
        return idCopy;
    }

    public int getDonation() {
        return donation;
    }

    public void setDonation(int donation) {
        this.donation = donation;
    }
}
