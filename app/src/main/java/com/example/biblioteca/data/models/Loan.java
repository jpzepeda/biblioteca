package com.example.biblioteca.data.models;

import java.util.Date;

public class Loan {
    private final int id;
    private final int idCopy;
    private final int idPerson;
    private final Date outDate;
    private Date inDate;

    public Loan(int id, int idCopy, int idPerson, Date outDate, Date inDate) {
        this.id = id;
        this.idCopy = idCopy;
        this.idPerson = idPerson;
        this.outDate = outDate;
        this.inDate = inDate;
    }

    public int getId() {
        return id;
    }

    public int getIdCopy() {
        return idCopy;
    }

    public int getIdPerson() {
        return idPerson;
    }

    public Date getOutDate() {
        return outDate;
    }

    public Date getInDate() {
        return inDate;
    }

    public void setInDate(Date inDate) {
        this.inDate = inDate;
    }
}
