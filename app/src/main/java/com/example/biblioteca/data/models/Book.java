package com.example.biblioteca.data.models;

import java.util.Date;

public class Book {
    private final int id;
    private String title;
    private final int idPublisher;
    private final int idLanguage;
    private int pages;
    private int isbn;
    private final Date entryDate;
    private int rare;
    private final int idAuthor;

    public Book(int id, String title, int idPublisher, int idLanguage, int pages, int isbn, Date entryDate, int rare, int idAuthor) {

        this.id = id;
        this.title = title;
        this.idPublisher = idPublisher;
        this.idLanguage = idLanguage;
        this.pages = pages;
        this.isbn = isbn;
        this.entryDate = entryDate;
        this.rare = rare;
        this.idAuthor = idAuthor;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIdPublisher() {
        return idPublisher;
    }

    public int getIdLanguage() {
        return idLanguage;
    }

    public int getIdAuthor() {
        return idAuthor;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public int getRare() {
        return rare;
    }

    public void setRare(int rare) {
        this.rare = rare;
    }

}
