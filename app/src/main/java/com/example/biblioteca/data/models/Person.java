package com.example.biblioteca.data.models;


import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

public class Person {
    private final int id;
    private String name;
    private String lastName1;
    private String lastName2;
    private String claveElector;
    private final int idState;
    private final Date birthDate;
    private final Date registerDate;
    private String phone;
    private String email;
    private String hashedPassword;

    public Person(int id, String name, String lastName1, String lastName2, String claveElector, int idState, Date birthDate, Date registerDate, String phone, String email, String hashedPassword) {
        this.id = id;
        this.name = name;
        this.lastName1 = lastName1;
        this.lastName2 = lastName2;
        this.claveElector = claveElector;
        this.idState = idState;
        this.birthDate = birthDate;
        this.registerDate = registerDate;
        this.phone = phone;
        this.email = email;
        if(id == 0)
        this.setHashedPassword(hashedPassword);
        else
        this.hashedPassword = hashedPassword;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName1() {
        return lastName1;
    }

    public void setLastName1(String lastName1) {
        this.lastName1 = lastName1;
    }

    public String getLastName2() {
        return lastName2;
    }

    public void setLastName2(String lastName2) {
        this.lastName2 = lastName2;
    }

    public String getClaveElector() {
        return claveElector;
    }

    public void setClaveElector(String claveElector) {
        this.claveElector = claveElector;
    }

    public int getIdState() {
        return idState;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(@NotNull String plainTextPass) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(plainTextPass.getBytes());
            byte[] messageDigest = digest.digest();
            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            this.hashedPassword = hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Log.e("Algoritm exeption",e.getMessage());
            this.hashedPassword = "Parse Error";
        }
    }

    public String getHashedPasswordToConfirm() {
        return this.hashedPassword;
    }
}
