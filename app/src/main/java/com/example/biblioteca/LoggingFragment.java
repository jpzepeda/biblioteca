package com.example.biblioteca;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.icu.util.GregorianCalendar;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.biblioteca.data.DataManager;
import com.example.biblioteca.data.models.Log;
import com.example.biblioteca.ui.adapters.ListAdapterLogging;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoggingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoggingFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public LoggingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoggingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoggingFragment newInstance(String param1, String param2) {
        LoggingFragment fragment = new LoggingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_logging, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SharedPreferences sharedPref = requireActivity().getPreferences(Context.MODE_PRIVATE);
        final NavController navController = Navigation.findNavController(view);
        if (sharedPref.contains(getString(R.string.date))) {
            String string = sharedPref.getString(getString(R.string.date), "1998-08-24");
            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            java.text.SimpleDateFormat formatLeft = new java.text.SimpleDateFormat("mm:ss");
            Calendar calendarNow = Calendar.getInstance();
            Calendar calendarStored = new GregorianCalendar();
            android.util.Log.i("computed time", calendarNow.getTime().toString());
            try {
                Date dateStored = format.parse(string);
                android.util.Log.i("stored time", Objects.requireNonNull(dateStored).toString());
                calendarStored.setTime(dateStored);
                //change lapse beetwen shows in here
                calendarStored.add(Calendar.MINUTE, 5);
                if (!(calendarNow.compareTo(calendarStored) >= 0)) {
                    Date timeToWait = new Date(calendarStored.getTimeInMillis() - calendarNow.getTimeInMillis());
                    String computedRest = formatLeft.format(timeToWait);
                    String response = getString(R.string.please_wait)+computedRest;
                    Snackbar.make(view, response, Snackbar.LENGTH_LONG)
                            .setAction(R.string.action, null).show();
                    android.util.Log.i("executing","edge");
                    navController.navigate(R.id.nav_authors);
                    return;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        init(view);
/*
        Timer timer = new Timer();
        class closeFragmentTask extends TimerTask {
            public void run() {
                requireActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final NavController navController = Navigation.findNavController(view);
                        navController.navigate(R.id.nav_authors);
                        android.util.Log.i("Stoped","time its over");
                    }
                });
            }
        }
        final long FPS = 99999999;
        TimerTask closeFrgament = new closeFragmentTask();
        timer.scheduleAtFixedRate(closeFrgament,0,FPS);
*/


        //change time to show in here
        new CountDownTimer(3000, 100) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                requireActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final NavController navController = Navigation.findNavController(view);
                        navController.navigate(R.id.nav_authors);
                        android.util.Log.i("Stoped", "time its over");
                        SharedPreferences sharedPref = requireActivity().getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        editor.putString(getString(R.string.date), dateFormat.format(Calendar.getInstance().getTime()));
                        editor.apply();
                        Snackbar.make(view, R.string.time_over, Snackbar.LENGTH_LONG)
                                .setAction(R.string.action, null).show();
                    }
                });
            }
        }.start();
    }

    private void init(View view) {
        final DataManager dataManager = new DataManager(requireActivity());
        ArrayList<Log> logs = dataManager.getAllLogs();
        ListAdapterLogging listAdapterLogging = new ListAdapterLogging(logs, getActivity());
        RecyclerView recyclerView = view.findViewById(R.id.loggingReciclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(listAdapterLogging);
    }
}